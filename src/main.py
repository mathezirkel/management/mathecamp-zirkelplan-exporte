#!/usr/bin/python3
# -*- coding: utf-8 -*-

# verwaltung
# Copyright (C) 2023  Mathezirkel Augsburg

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

#warnings.filterwarnings("ignore", category=FutureWarning)
#pd.set_option("display.max.rows", None)

import os
import yaml
import json
import numpy as np
import pandas as pd
import openpyxl
import codecs
from pathlib import Path

from mc_beamer_allocation import *
from mc_certificates import *
from mc_contacts import *
from mc_social_graph import *
from mc_timetable import *
from mc_zirkelplan import *
from mc_chiefs import *

def main() -> None:
    MC_LIST_DIR = Path(os.getenv("MC_LIST_DIR"))
    MC_DOCUMENT_DIR = Path(os.getenv("MC_DOCUMENT_DIR"))
    MC_CONFIG_DIR = Path(os.getenv('MC_CONFIG_DIR'))

    database_url = "https://app.mathezirkel-augsburg.de/graphql"

    # Open config file
    config_file = MC_CONFIG_DIR / "config.yaml"
    with open(config_file, 'r') as configFile:
        config = yaml.safe_load(configFile)

    # Event ID
    event_id = config.get("event_id", "")

    #Auth header
    auth_header = json.loads(config.get("auth_header", ""))

    # Social graph
    for item in config.get("social_graph",[]):
        export_dir = MC_DOCUMENT_DIR / item["output_dir"]
        if not export_dir.exists():
            export_dir.mkdir(parents=True, exist_ok=True)
        input_file = MC_LIST_DIR / item["input"]
        with open(input_file, 'r') as file:
            data = json.load(file)
        write_social_graphs(data, export_dir)

    # Contacts
    for item in config.get("contacts", []):
        export_dir = MC_DOCUMENT_DIR / item["output_dir"]
        if not export_dir.exists():
            export_dir.mkdir(parents=True, exist_ok=True)
        output_file = export_dir / item["filename"]
        input_file = MC_LIST_DIR / item["input"]
        with open(input_file, 'r') as file:
            data = json.load(file)
        list_of_vcards = create_vcf(data)
        with codecs.open(output_file, 'w', 'utf-8-sig') as vcfFile:
            for card in list_of_vcards:
                vcfFile.write(card.serialize())

    # Read Zirkelplan
    sheet_id = config["zirkelplan"]["sheet_id"]
    sheet_name = config["zirkelplan"]["sheet_name"]
    url=f"https://docs.google.com/spreadsheets/export?id={sheet_id}&exportFormat=xlsx"
    skip_rows = [n - 1 for n in config["zirkelplan"]["skip_rows"]]
    extra_columns = config["zirkelplan"]["extra_columns"]

    # Parse Zirkelplan
    raw_zirkelplan = pd.read_excel(url, sheet_name=sheet_name, header=None, skiprows=skip_rows)
    raw_zirkelplan = preprocess_zirkelplan(raw_zirkelplan, extra_columns)

    zirkelplan_dir = MC_DOCUMENT_DIR / "zirkelplan"
    if not zirkelplan_dir.exists():
        zirkelplan_dir.mkdir(parents=True, exist_ok=True)
    raw_zirkelplan.to_csv(zirkelplan_dir / "raw.csv", sep=";")
    print(f"Created: {zirkelplan_dir}/raw.csv")

    teachers = pd.read_csv(MC_LIST_DIR / config["data"]["teachers"], sep=';')
    slots = pd.read_csv(MC_CONFIG_DIR / config["data"]["slots"], sep=';')

    # Zirkelplan
    zirkelplan = get_zirkelplan(raw_zirkelplan, teachers["Kuerzel"], slots["Slot"], config["zirkelplan"]["choice_system"])
    zirkelplan.to_csv(zirkelplan_dir / "zirkelplan.csv", sep=";")
    print(f"Created: {zirkelplan_dir}/zirkelplan.csv")
    print("Read: Zirkelplan")

    # Rooms
    rooms = get_rooms(raw_zirkelplan, "Raum", extra_columns)
    room_dir = MC_DOCUMENT_DIR / config["rooms"]["output_dir"]
    if not room_dir.exists():
        room_dir.mkdir(parents=True, exist_ok=True)
    rooms.to_csv(room_dir / "zirkel.csv", sep=';', index=False)
    print(f"Created: {room_dir}/zirkel.csv")
    if config["zirkelplan"]["infozirkel"]:
        rooms_infozirkel = (
            get_rooms(raw_zirkelplan, f"Raum Infozirkel", extra_columns)
            # Add teachers to dataframe
            .merge(zirkelplan[zirkelplan["Slot"] == "Infozirkel"][["Zirkel", "Zirkelleitende"]], how="left", on="Zirkel")
        )
        rooms_infozirkel['Zirkelleitende'] = rooms_infozirkel['Zirkelleitende'].apply(lambda x: ', '.join(x))
        rooms_infozirkel.to_csv(room_dir / "infozirkel.csv", sep=';', index=False)
        print(f"Created: {room_dir}/infozirkel.csv")
    else:
        rooms_infozirkel = None

    # Certificates
    if config["certificates"]["enabled"]:
        print("Write topics to database")
        write_topics_to_database(
            zirkelplan,
            slots,
            config["zirkelplan"]["choice_system"],
            database_url,
            auth_header,
            event_id
        )

    # Chiefs
    all_chiefs = []
    if config["chiefs"]["enabled"]:
        chiefs_file = MC_DOCUMENT_DIR / config["chiefs"]["html_file"]
        with codecs.open(chiefs_file) as file:
            chief_list_html = file.read()
        output_dir = output_file = MC_LIST_DIR / config["chiefs"]["output"]
        if not output_dir.exists():
            output_dir.mkdir(parents=True, exist_ok=True)
        for chief_class in ["chiefs", "fraktivitaeten_chiefs"]:
            heading_id = config["chiefs"]["heading_id"][chief_class]
            chiefs_of_class = get_list_of_chiefs(chief_list_html, heading_id, teachers["Kuerzel"])
            all_chiefs.append(chiefs_of_class)
            chiefs_of_class["Betreuer:innen"] = chiefs_of_class["Betreuer:innen"].apply(lambda x: ", ".join(x))
            chiefs_of_class.to_csv(output_dir / f"{chief_class}.csv", sep=';', index=False)
            print(f"Created: {output_dir}/{chief_class}.csv")

    # Timetable
    if config["timetable"]["enabled"]:
        campdays = pd.read_csv(MC_CONFIG_DIR / config["data"]["campdays"], sep=';', index_col=0)
        output_file = MC_DOCUMENT_DIR / config["timetable"]["output"]
        if not output_file.parent.exists():
            output_file.parent.mkdir(parents=True, exist_ok=True)
        with codecs.open(output_file, 'wb','utf-8-sig') as timetable_file:
            teX_code = make_timetable(zirkelplan, teachers["Kuerzel"], rooms, rooms_infozirkel, slots, campdays, all_chiefs)
            timetable_file.write(teX_code)
        print(f"Created: {output_file}")

    # Beamer
    if config["beamer"]["enabled"]:
        campdays = pd.read_csv(MC_CONFIG_DIR / config["data"]["campdays"], sep=';', index_col=0)
        room_change_penalty_matrix = pd.read_csv(MC_CONFIG_DIR / config["data"]["room_change_penalties"], sep=';', index_col=0)
        output_file = MC_DOCUMENT_DIR / config["beamer"]["output"]
        if not output_file.parent.exists():
            output_file.parent.mkdir(parents=True, exist_ok=True)
        with codecs.open(output_file, 'wb','utf-8-sig') as beamer_file:
            # In case of multiple rooms use only the first one
            rooms_adjusted = rooms.copy()
            rooms_adjusted["Raum"] = rooms_adjusted["Raum"].apply(lambda x: x.split(",")[0])
            print(rooms_adjusted)
            beamer_list = make_beamer_list(zirkelplan, slots, rooms_adjusted, campdays, config["beamer"]["available"], room_change_penalty_matrix)
            teX_code = make_beamer_list_tex(beamer_list)
            beamer_file.write(teX_code)
        print(f"Created: {output_file}")

if __name__ == "__main__":
    main()
