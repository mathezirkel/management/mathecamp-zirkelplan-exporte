#!/usr/bin/python3
# -*- coding: utf-8 -*-

# verwaltung
# Copyright (C) 2023  Mathezirkel Augsburg

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from typing import List, Dict, Union
import pandas as pd
import numpy as np
import re

def preprocess_cell(cell: List[str]) -> List[List[str]]:
    """
    This function takes in a cell of the shared spreadsheet containing the content of a specific Zirkel.
    These information is prepocessed for further analysis.

    Parameters:
        cell (List[str]): The content of the subcells of a certain Zirkel.

    Returns:
        List[List[str]]: The content in a form to be processed further
    """
    # Remove nan's
    cell = [s for s in cell if pd.notna(s)]
    # Get unique entries
    cell = list(set(cell))
    # Remove &'s which are used for convenience
    cell = [s for s in cell if s.strip() != "&"]
    # Split the cells by newlines, since each line has a meaning
    cell = [s.split('\n') for s in cell]
    # Remove trailing whitespaces
    cell = [[s.strip() for s in subcell] for subcell in cell]
    return cell

def intepret_cell_content(cell: List[List[str]], teachers_id: pd.Series) -> Dict:
    """
    This function interprets the content of a preprocessed cell.

    Parameters:
        cell (List[str]): The preprocessed cell representing a Zirkel.
        teachers (pd.Series): A Series containing the ids of the teachers.

    Returns:
        List[List[str]]: A dict containing the keys containing the information of the Zirkel.
        The keys are ["Zirkelleitende", "Thema", "Beamer"].
    """
    teacher = []
    topic = ""
    beamer = False

    # Get names of the teacher
    for name in teachers_id:
        for subcell in cell:
            if name in subcell[0] and name not in teacher:
                teacher.append(name)

    entered_topic = [subcell[1] for subcell in cell if len(subcell) > 1]
    if len(set(entered_topic)) > 1:
        raise ValueError(f"Zirkelplan inkonsistent - mehr als ein Thema angegeben: {entered_topic}")
    elif len(set(entered_topic)) == 1:
        topic = entered_topic[0].strip()

    for subcell in cell:
        if "beamer" in subcell[0].strip().lower():
            beamer = True
            break
        if len(subcell) > 2:
            if "beamer" in subcell[2].strip().lower():
                beamer = True
                break

    return {"Zirkelleitende": teacher, "Thema": topic, "Beamer": beamer}

def preprocess_zirkelplan(raw_zirkelplan: pd.DataFrame, extra_columns: List[str]) -> pd.DataFrame:
    """
    This function takes the raw Zirkelplan in the online shared spreadsheat and preprocesses it for further analysis.

    Parameters:
        raw_zirkelplan (pd.DataFrame): The content of the online shared spreadsheet representing the Zirkelplan.
        extra_columns (List[str]): The columns which do not represent a Zirkel.

    Returns:
        pd.DataFrame: The preprocessed Zirkelplan.
    """
    tmp_df = (
        raw_zirkelplan
        .copy()
        .dropna(axis=0, how='all')
        .reset_index(drop=True)
        .dropna(axis=1, how='all')
    )
    tmp_df[tmp_df.columns[0]] = (
        tmp_df[tmp_df.columns[0]]
        .fillna(method='ffill', axis=0)
        # Replace newline by space within the index columns
        .apply(lambda x: x.replace("\n", " "))
    )
    tmp_df.loc[0] = tmp_df.loc[0].fillna(method='ffill')
    tmp_df = tmp_df.rename(columns=tmp_df.iloc[0])
    tmp_df = tmp_df.drop(tmp_df.index[0])
    tmp_df = tmp_df.reset_index(drop=True)
    tmp_df = tmp_df.set_index(tmp_df.columns[0])

    pre_zirkelplan = pd.DataFrame()
    for col in tmp_df.columns.unique():
        pre_zirkelplan[col] = tmp_df[[col]].apply(lambda x: x.tolist(), axis=1)

    pre_zirkelplan = pre_zirkelplan.applymap(preprocess_cell)

    cols_to_fill = [col for col in pre_zirkelplan.columns if col not in extra_columns]

    # A bit hacky - replace [] by nan to be able to fill them
    pre_zirkelplan[cols_to_fill] = (
        pre_zirkelplan[cols_to_fill]
        .applymap(lambda x: x if x else np.nan)
    )
    # Fill rooms first per along x-axis
    room_indexes = [index for index in ["Raum", "Raum Infozirkel"] if index in pre_zirkelplan.index]
    pre_zirkelplan.loc[room_indexes, cols_to_fill] = (
        pre_zirkelplan[cols_to_fill]
        .loc[room_indexes]
        .fillna(method='ffill', axis=1)
    )
    # Fill lessons first along x-axis then along y-axis
    for col in cols_to_fill:
        pre_zirkelplan[[col]] = (
            pre_zirkelplan[[col]]
            .fillna(method='ffill', axis=1)
            .fillna(method='ffill', axis=0)
        )
    pre_zirkelplan = pre_zirkelplan.groupby(level=0, sort=False).sum()
    return pre_zirkelplan

def validate_room_entry(raw_data: List[List[str]]) -> bool:
    """
    This functions validates if the room entry is correct.

    Parameters:
        raw_data (List[str]): The preprocessed cell of the online shared spreadsheet containing the room information.

    Returns:
        bool: Whether the room entry is valid.
    """
    # Check whether there is exactly one sublist
    if len(raw_data) != 1:
        return False
    # Check whether the sublists contain exactly one element
    for subentry in raw_data:
        if len(subentry) != 1:
            return False
    return True

def get_room_entry(raw_data: List[List[str]]) -> str:
    """
    The funtions gets the room from the preprocessed cell containing the room information.

    Parameters:
        raw_data (List[str]): The preprocessed cell of the online shared spreadsheet containing the room information.

    Returns:
        str: The name of the room of the corresponding Zirkel.
    """
    if validate_room_entry(raw_data):
        return raw_data[0][0]
    else:
        raise ValueError("There is more than one room assigned.")

def get_grade_range(grades: Union[str, int, np.ndarray]) -> str:
    """
    The function returns the range of the given grades as a string.

    Parameters:
        grades (Union[str, int, np.ndarray]): The given grades.

    Returns:
        str: The grades represented as a string.
    """
    if isinstance(grades, str) or isinstance(grades, int):
        return str(grades)
    elif isinstance(grades, np.ndarray):
        # Get a list of all grades
        range_grades = sorted(set([re.sub('[a-z]', '', str(x)) for x in grades.tolist()]))
        if len(range_grades) == 1:
            return range_grades[0]
        else:
            return f"{range_grades[0]}--{range_grades[-1]}"
    else:
        raise ValueError(f"Unknown data type: Value: {grades}, Type: {type(grades)}")


def get_rooms(pre_zirkelplan: pd.DataFrame, index_name: str, extra_columns: List[str]) -> pd.DataFrame:
    """
    The function creates a DataFrame containing the room of each Zirkel from the preprocessed Zirkelplan.

    Parameters:
        pre_zirkelplan (pd.DataFrame): The preprocessed Zirkelplan.
        index_name (str): The index containing the information about the rooms.
        extra_columns (List[str]): The columns which do not represent a Zirkel.

    Returns:
        pd.DataFrame: A DataFrame containing the room for each Zirkel.
    """
    zirkel_cols = [col for col in pre_zirkelplan.columns if col not in extra_columns]
    rooms = pre_zirkelplan[zirkel_cols].loc[[index_name],:].T
    for zirkel in rooms.index:
        try:
            rooms.at[zirkel, index_name] = get_room_entry(rooms.at[zirkel, index_name])
        except ValueError as e:
            raise ValueError(f"Error in Zirkel {zirkel}: {e}")
    rooms = rooms.reset_index(names=["Zirkel"])
    rooms = rooms.groupby(index_name, sort=False).agg(lambda x: x)
    rooms = rooms.reset_index()
    rooms[["Zirkel"]] = rooms[["Zirkel"]].applymap(get_grade_range)
    rooms = rooms[["Zirkel", index_name]]
    return rooms

def get_zirkelplan(pre_zirkelplan: pd.DataFrame, teachers: pd.Series, slots: pd.Series, choice_system: Dict[str,List[str]]) -> pd.DataFrame:
    """
    The function creates a table containg all Zirkel including the following information: ["Slot", "Zirkel", "Thema", "Zirkelleitende", "Beamer"]

    Parameters:
        pre_zirkelplan (pd.DataFrame): The preprocessed Zirkelplan.
        teachers (pd.Series): A Series containing the ids of the teachers.
        slots (pd.DataFrame): The DataFrame containing the information about the slots.
        choice_system (Dict[str,List[str]]) :Dictionary containing the ZirkelIDs and the corresponding columns in the Zirkelplan.

    Returns:
        pd.DataFrame: The table of all Zirkel of the Mathecamp.
    """
    zirkelplan = pd.DataFrame(columns = ["Slot", "Zirkel", "Thema", "Zirkelleitende", "Beamer"]).astype({"Beamer": bool})
    # Iterate over all defines slots
    for slot in slots:
        for zirkel, cell in pre_zirkelplan.loc[slot].items():
            if cell:
                cell_content = intepret_cell_content(cell, teachers)
                new_row = pd.DataFrame([{"Slot": slot, "Zirkel": zirkel, **cell_content}]).astype({"Beamer": bool})
                zirkelplan = pd.concat([zirkelplan, new_row], ignore_index = True)
    # Zirkel within the choice system get the same identifier
    for zirkelID, corresponding_columns in choice_system.items():
        zirkelplan["Zirkel"] = (
            zirkelplan["Zirkel"]
            .apply(lambda x: zirkelID if x in corresponding_columns else x)
        )
    # Infozirkel and Aufräumen
    zirkelplan.loc[zirkelplan["Slot"].isin(["Infozirkel", "Sonntag (Aufräumen)"])] = (
        zirkelplan
        .loc[zirkelplan["Slot"].isin(["Infozirkel", "Sonntag (Aufräumen)"])]
        .groupby(["Slot", "Zirkel"], sort=False)
        .agg({
            "Thema": "first",
            "Zirkelleitende": (lambda x: list(set(item for sublist in x for item in sublist))),
            "Beamer" : (lambda x: any(x))
        })
        .reset_index()
    )
    zirkelplan = (
        zirkelplan
        .dropna(axis=0, how='all')
        .reset_index()
    )
    # Lessons containing more than one Zirkel
    zirkelplan["tmp"] = zirkelplan["Zirkelleitende"].apply(lambda x: "".join(x))
    zirkelplan = (
        zirkelplan
        .groupby(["Slot", "tmp"], sort=False)
        .agg({
            "Zirkel" : (lambda x: x),
            "Thema": "first",
            "Zirkelleitende": "first",
            "Beamer" : "first"
        })
        .reset_index()
        .drop(columns=["tmp"])
    )
    zirkelplan["Zirkel"] = zirkelplan["Zirkel"].apply(get_grade_range)
    return zirkelplan
