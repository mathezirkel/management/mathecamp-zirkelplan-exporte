#!/usr/bin/python3
# -*- coding: utf-8 -*-

# verwaltung
# Copyright (C) 2023  Mathezirkel Augsburg

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import codecs
import numpy as np
import json
import pandas as pd
import requests
import sys

def make_list_of_topics(zirkelplan: pd.DataFrame, slots: pd.DataFrame, choice_system: dict[str,list[str]]) -> pd.DataFrame:
    """
    This function returns a DataFrame containing a comma-separated list of the topics for each Zirkel.

    Parameters:
        zirkelplan (pd.DataFrame): The DataFrame containing the Zirkelplan.
        slots (pd.DataFrame): The DataFrame containing the information about the slots.
        choice_system (Dict[str,List[str]]): Dictionary containing the ZirkelIDs and the corresponding columns in the Zirkelplan.

    Returns:
        pd.DataFrame: The DataFrame containing the comma-separated list of the topics covered for each Zirkel.
    """
    adjusted_zirkelplan = zirkelplan.copy()
    for zirkelID, corresponding_columns in choice_system.items():
        adjusted_zirkelplan["Zirkel"] = zirkelplan["Zirkel"].apply(lambda x: zirkelID if x in corresponding_columns else x)
    list_of_topics = (
        adjusted_zirkelplan[["Slot", "Zirkel", "Thema"]]
        .merge(slots[["Slot", "Urkunde"]], left_on="Slot", right_on="Slot")
        .query("Urkunde == True")
        [["Zirkel", "Thema"]]
        .groupby("Zirkel", sort=False)
        .agg(lambda x: list(set([i for i in x if i])))
        .rename(columns={"Thema": "Themen"})
    )
    return list_of_topics

def get_zirkels_ids(url: str, auth_header: str, event_id: str) -> dict:
    query = f"""
    query GetZirkel {{
        event(id: "{event_id}") {{
            zirkel {{
                id
                name
            }}
        }}
    }}
    """
    json_query = {
        "query": query
    }
    response = requests.post(
        url,
        json=json_query,
        headers=auth_header,
    )
    # Check if the request was successful
    if response.status_code == 200:
        data = response.json()
        return data
    else:
        print(f"Failed to read {response.status_code}: {response.text}", file=sys.stderr)
        return {}

def make_graphql_list(input: list[str]) -> str:
    return str(input).replace("'", '"')

def write_topics_to_database(zirkelplan: pd.DataFrame, slots: pd.DataFrame, choice_system: dict[str,list[str]], url: str, auth_header: str, event_id: str) -> None:
    list_of_topics = make_list_of_topics(zirkelplan, slots, choice_system)
    zirkel_ids = get_zirkels_ids(url, auth_header, event_id)
    for zirkel in zirkel_ids["data"]["event"]["zirkel"]:
        uuid = zirkel["id"]
        name = zirkel["name"]
        topics = list_of_topics.loc[name, 'Themen']
        query = f"""
        mutation WriteTopic {{
            updateZirkel(
                values: {{topics: {make_graphql_list(topics)}}}
                id: "{uuid}"
            ) {{
                topics
            }}
        }}
        """
        json_query = {
            "query": query,
        }
        response = requests.post(
            url,
            json=json_query,
            headers=auth_header,
        )
        # Check if the request was successful
        if response.status_code == 200:
            print(response.json())
        else:
            print(f"Failed to create {response.status_code}: {response.text}", file=sys.stderr)
