import vobject

def create_card(entry: dict[str, str | list[str] | list[dict[str,str]]]):
    card = vobject.vCard()

    # Name
    n = card.add("N")
    family_name = entry.get("familyName", "")
    call_name = entry.get("callName", "")
    n.value = vobject.vcard.Name(
        family=family_name,
        given=call_name,
    )

    # Formatted Name (required)
    fn = card.add("FN")
    formatted_name = f"{call_name} {family_name}"
    fn.value = formatted_name

    # Address
    if address := entry.get("address", {}):
        address_field = card.add("ADR")
        address_field.value = vobject.vcard.Address(
            street=f"{address.get('street', '')} {address.get('streetNumber', '')}",
            code=address.get("postalCode", ""),
            city=address.get("city", ""),
            country=address.get("country", ""),
        )

    # Org
    if class_year := entry.get("classYear", ""):
        org_field = card.add("ORG")
        org_field.value = [f"Klasse {class_year}"]

    # E-Mails
    if email := entry.get("emailSelf", ""):
        email_field = card.add("EMAIL")
        email_field.value = email
        email_field.type_param = "HOME"
    if email := entry.get("email", ""):
        email_field = card.add("EMAIL")
        email_field.value = email
        email_field.type_param = "HOME"
    for email in entry.get("additionalEmails", []):
        email_field = card.add("EMAIL")
        email_field.value = email
        email_field.type_param = "OTHER"

    # Telefon
    if telephone := entry.get("telephone", ""):
        telephone_field = card.add("TEL")
        telephone_field.value = telephone
        telephone_field.type_param = "HOME"
    for contact in entry.get("contacts", []):
        telephone_field = card.add("TEL")
        telephone_field.value = contact["telephone"]
        telephone_field.type_param = "X-NOTFALL"

    return card

def create_vcf(data: list[dict[str, str | list[str] | list[dict[str,str]]]]) -> list[vobject.vCard]:
    """
    Create a list of vCards from Mathezirkel App export.

    Parameters:
        data: The data to be converted into vcards.

    Returns:
        List[vobject.vCard]: A list of vCard objects created from the data.
    """

    list_of_vcards = [create_card(entry) for entry in data]

    return list_of_vcards
