#!/usr/bin/python3
# -*- coding: utf-8 -*-

# verwaltung
# Copyright (C) 2023  Mathezirkel Augsburg

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import numpy as np
import pandas as pd
from typing import List
from mc_chiefs import get_chief_tasks


def make_timetable(zirkelplan: pd.DataFrame, teachers: pd.Series, rooms: pd.DataFrame, rooms_infozirkel: pd.DataFrame, slots: pd.DataFrame, campdays: pd.DataFrame, all_chiefs : List[pd.DataFrame] = []) -> str:
    """
    This function creates a timetable for each teacher in TeX.

    Parameters:
        zirkelplan (pd.DataFrame): The DataFrame containing the Zirkelplan.
        teachers (pd.DataFrame): The DataFrame containing the list of teacher.
        rooms (pd.DataFrame): The DataFrame containing the information about the rooms.
        rooms_infozirkel (pd.DataFrame): The DataFrame containing the information about the rooms of the extra_zirkel or None.
        slots (pd.DataFrame): The DataFrame containing the information about the slots.
        campdays (pd.DataFrame): The DataFrame containing the information about the campdays.
        all_chiefs (List[pd.DateFrame]): The DataFrames containing the chiefs and fraktivitäten-chiefs. If [] (default), chief task will not be added.

    Returns:
        str: TeX code to create a timetable for each teacher.
    """
    output = ""
    for teacher in teachers:
        output += \
rf"""\makeheader

\begin{{center}}
    \Huge\textsc{{Stundenplan}}
\end{{center}}

\begin{{tabular}}{{ll}}
    \textbf{{Name:}} & {teacher}
\end{{tabular}}

\vspace{{-0.5cm}}

"""

        timetableTeX = \
r"""\begin{center}
\rowcolors{1}{gray!20}{white}
\begin{xltabular}{\textwidth}{|l|l|c|l|X|X|}
    \hline
    \rowcolor{gray!40}
    \textbf{Tag} & \textbf{Zeit} & \textbf{Zirkel} & \textbf{Zirkelleiter:innen} & \textbf{Raum} & \textbf{Thema}\\
    \hline
    \endhead
"""
        for _, slot in slots.iterrows():
            slotname = slot["Slot"]
            # Get all rows of the corresponding slot
            entry = zirkelplan.query("Slot == @slotname")
            # Get all rows of the corresponding teacher
            entry = entry[entry["Zirkelleitende"].apply(lambda x: teacher in x)]
            if not entry.empty:
                # Convert to a series
                entry = entry.iloc[0]
                # Current day
                current_day = campdays.at[slot["Camptag"],"Wochentag"]
                # Current timeslot
                current_time = slot["Zeitraum"]
                # Current Zirkel
                current_zirkel = entry["Zirkel"]
                # Current teacher
                current_teachers = ", ".join(entry["Zirkelleitende"])
                # Current room
                current_room = ""
                if rooms_infozirkel is not None and "Infozirkel" in slotname:
                    room_query = rooms_infozirkel.query("Zirkel == @current_zirkel")
                    if not room_query.empty:
                        current_room = room_query.iloc[0][f"Raum Infozirkel"]
                else:
                    room_query = rooms.query("Zirkel == @current_zirkel")
                    if not room_query.empty:
                        current_room = room_query.iloc[0]["Raum"]
                # Current topic
                if "Infozirkel" in slotname:
                    current_topic = "Infozirkel"
                elif "Aufräumen" in slotname:
                    current_topic = "Aufräumen"
                else:
                    current_topic = entry["Thema"]
                    if entry["Beamer"]:
                        current_topic += " (Beamer)"

                # Write TeX code
                timetableTeX += rf"    {current_day} & {current_time} & {current_zirkel} & {current_teachers} & {current_room} & {current_topic}\\ \hline" + "\n"
        timetableTeX += \
r"""
\end{xltabular}
\end{center}
"""
        output += timetableTeX

        if all_chiefs:
            chief_tasks_of_current_teacher = get_chief_tasks(teacher, all_chiefs[0])
            fraktivitaeten_chief_tasks_of_current_teacher = get_chief_tasks(teacher, all_chiefs[1])
            output += \
rf"""
\textbf{{Deine Chief-Aufgaben}}: {chief_tasks_of_current_teacher}

\textbf{{Deine Fraktivitäten}}: {fraktivitaeten_chief_tasks_of_current_teacher}

"""
        output += \
r"""\clearpage

"""
    return output
