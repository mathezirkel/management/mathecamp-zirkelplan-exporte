#!/usr/bin/python3
# -*- coding: utf-8 -*-

# verwaltung
# Copyright (C) 2023  Mathezirkel Augsburg

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import numpy as np
import pandas as pd
from bs4 import BeautifulSoup
from typing import List

def get_chief_tasks(teacher: str, chiefs: pd.DataFrame) -> str:
    """
    This function return all chief tasks of a given teacher.

    Parameters:
        teacher (str): ID of the teacher
        chiefs (pd.DataFrame): DataFrame containing the chief tasks and the responsible teachers.

    Returns:
        str: List of chief tasks of the given teacher.
    """
    chief_task_of_teacher = chiefs[chiefs["Betreuer:innen"].apply(lambda x: teacher in x)]
    return ", ".join(chief_task_of_teacher["Chief"].tolist())

def parse_pad(pad_html: str, id_heading : str) -> List[str]:
    """
    This function returns the strings of an unorderded list underneath a certain
    heading of an HTML document.

    Parameters:
        pad_html (str): HTML string of the HedgeDoc pad
        id_heading (str): ID of the heading where the list of chiefs is located.

    Returns:
        List[str]: List of the strings
    """
    soup = BeautifulSoup(pad_html, "html.parser")
    heading = soup.find(id=id_heading)
    chief_list_html = heading.find_next_sibling("ul")
    chiefs_list_unparsed = [li.get_text() for li in chief_list_html.find_all("li")]
    return chiefs_list_unparsed

def get_list_of_chiefs(pad_html: str, id_heading : str, teachers: pd.Series) -> pd.DataFrame:
    """
    This function returns a DataFrame of the chief tasks with the responsible teachers as a list.
    Each element of the parameter chiefs_list_parsed is of the form
    chief_task:some_text_containing_responsible_teachers. The function splits at the last
    occurence of ":".

    Parameters:
        pad_html (str): HTML string of the HedgeDoc pad
        id_heading (str): ID of the heading where the list of chiefs is located.
        teachers (pd.Series): List of all teachers

    Returns:
        pd.DataFrame: The DataFrame the chiefs and responsible teachers.
    """
    chiefs_list_unparsed = parse_pad(pad_html, id_heading)
    row = []
    for entry in chiefs_list_unparsed:
        chief, teachers_string = entry.split(":",maxsplit=1)
        teachers_per_chief = []
        for teacher in teachers:
            if teacher in teachers_string:
                teachers_per_chief.append(teacher)
        row.append([chief, teachers_per_chief])
    chiefs = pd.DataFrame(row, columns=["Chief", "Betreuer:innen"]).sort_values(by = "Chief")
    return chiefs
