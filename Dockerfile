FROM python:3.11.2-buster

# Set working directory
WORKDIR /app

# Copy requirements.txt into image and install dependencies
COPY requirements.txt /app
RUN pip install --no-cache-dir -r requirements.txt

# Define volumes
VOLUME /app/lists
VOLUME /app/documents
VOLUME /app/config

# Copy source files into image
COPY src /app/src

# Set non-root user
RUN useradd -ms /bin/bash user
RUN chown -R user /app
USER user

# Set environment variables
ENV MC_LIST_DIR=/app/lists
ENV MC_DOCUMENT_DIR=/app/documents
ENV MC_CONFIG_DIR=/app/config

# Set entrypoint
CMD ["python", "src/main.py"]
