#!/usr/bin/python3
# -*- coding: utf-8 -*-

# verwaltung
# Copyright (C) 2023  Mathezirkel Augsburg

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import numpy as np
import pandas as pd
import networkx as nx
from typing import Dict

# Indices of special nodes
SOURCE_NODE = 100
TEAMZIMMER_NODE_1 = 200
TEAMZIMMER_NODE_2 = 300
DESTINATION_NODE = 400

def make_beamer_allocation_graph(classes_with_beamer: pd.DataFrame, room_change_penalty_matrix: pd.DataFrame, beamer_available: int) -> nx.DiGraph:
    """
    Every day beamers are needed in some Zirkel. The goal is to find an optimal beamer distribution. To do this, we model the list of required beamers as a graph.

    The graph consists of two layers and a start as well as a target node. Each node in the first resp. second layer represents a Zirkel that requires a beamer in the first resp. second slot. Additionally, there is one node in each layer for the team room to store surplus beamers.

    Parameters:
        classes_with_beamer (pd.DataFrame): The DataFrame containing the list of Zirkel requiring a beamer.
        room_change_penalty_matrix (pd.DataFrame): The penalties for moving a beamer from one to another room.
        beamer_available (int): The amount of available beamers.

    Returns:
        pd.DataFrame: The graph modelling the given beamer list.
    """
    # Initialize graph
    G = nx.DiGraph()
    # Add source and destination node
    G.add_node(SOURCE_NODE)
    G.add_node(DESTINATION_NODE)
    # Add nodes for the team room
    G.add_node(TEAMZIMMER_NODE_1, Layer=1, **{"Raum": "Teamzimmer", "Zirkelleitende": [], "Zirkel": ""})
    G.add_node(TEAMZIMMER_NODE_2, Layer=2, **{"Raum": "Teamzimmer", "Zirkelleitende": [], "Zirkel": ""})
    # Add nodes for the two layers and add an edge to the source resp. destination node
    for i, row in classes_with_beamer.iterrows():
        if '1' in row["Slot"]:
            G.add_node(i, Layer=1, **row.to_dict())
            G.add_edge(SOURCE_NODE, i, capacity=1, weight=0)
        if '2' in row["Slot"]:
            G.add_node(i, Layer=2, **row.to_dict())
            G.add_edge(i, DESTINATION_NODE, capacity=1, weight=0)
    # Get a list of nodes of each layer
    layer_1 = [node for (node,layer) in nx.get_node_attributes(G, "Layer").items() if layer == 1]
    layer_2 = [node for (node,layer) in nx.get_node_attributes(G, "Layer").items() if layer == 2]
    # Add an edge from the team room nodes to the source resp. destination nodes
    G.add_edge(SOURCE_NODE, TEAMZIMMER_NODE_1, capacity=(beamer_available - len(layer_1) + 1), weight=0)
    G.add_edge(TEAMZIMMER_NODE_2, DESTINATION_NODE, capacity=(beamer_available - len(layer_2) + 1), weight=0)
    # Add an edge from every node of layer 1 to layer 2
    for node_1 in layer_1:
        for node_2 in layer_2:
            rooms = nx.get_node_attributes(G, "Raum")
            penalty = room_change_penalty_matrix.at[rooms[node_1], rooms[node_2]]
            G.add_edge(node_1, node_2, capacity=1, weight=penalty)
    # Update capacity of edge between the team room nodes
    # Multiple beamers can stay in the team room
    nx.set_edge_attributes(G, {(TEAMZIMMER_NODE_1, TEAMZIMMER_NODE_2): {"capacity": 100}})
    # Update weights if teacher is the same
    # Prefer that the teacher can take the beamer to the next room
    teachers = nx.get_node_attributes(G, "Zirkelleitende")
    for edge in G.edges:
        if (edge[0] in teachers) and (edge[1] in teachers):
            if (set(teachers[edge[0]]) & set(teachers[edge[1]])):
                nx.set_edge_attributes(G, {edge: {"weight": 1}})
    return G

def get_beamer_allocation_by_graph(G: nx.DiGraph, beamer_available: int) -> pd.DataFrame:
    """
    The function finds the optimal beamer distribution applying the max-flow-min-costs algorithm to the graph modelling the beamer demand on a specific day.

    Parameters:
        G (nx.DiGraph): The graph modelling the beamer demand.
        beamer_available (int): The amount of available beamers.

    Returns:
        pd.DataFrame: The DataFrame containing the optimal usage of each beamer in each slot on a specific day.
    """
    optimized_flow_dict = nx.max_flow_min_cost(G, SOURCE_NODE, DESTINATION_NODE)
    beamer_allocation = pd.DataFrame(index=range(2), columns=range(beamer_available))
    counter=0
    for edge in list(G.edges(SOURCE_NODE)):
        if edge[1] == TEAMZIMMER_NODE_1:
            pass
        else:
            zirkel_1 = G.nodes[edge[1]]["Zirkel"]
            room_1 = G.nodes[edge[1]]["Raum"]
            teachers_1 = G.nodes[edge[1]]["Zirkelleitende"]
            beamer_allocation.at[0, counter] = {"Zirkel": zirkel_1, "Raum": room_1, "Zirkelleitende": teachers_1}
        for node, flow in optimized_flow_dict[edge[1]].items():
            if flow == 1:
                successive_node = node
                if successive_node == TEAMZIMMER_NODE_2:
                    counter += 1
                    continue
                zirkel_2 = G.nodes[successive_node]["Zirkel"]
                room_2 = G.nodes[successive_node]["Raum"]
                teachers_2 = G.nodes[successive_node]["Zirkelleitende"]
                beamer_allocation.at[1, counter] = {"Zirkel": zirkel_2, "Raum": room_2, "Zirkelleitende": teachers_2}
                counter += 1
    return beamer_allocation

def make_beamer_list(zirkelplan: pd.DataFrame, slots: pd.DataFrame, rooms: pd.DataFrame, campdays: pd.DataFrame, beamer_available: int, room_change_penalty_matrix: pd.DataFrame) -> pd.DataFrame:
    """
    The function creates an optimal beamer distribution for the Mathecamp. It takes into account only the Zirkel in the morning.
    Each cell contains either a Dict with the keys ["Zirkel", "Zirkelleitende", "Raum"] or is NaN.

    Parameters:
        zirkelplan (pd.DataFrame): The DataFrame containing the Zirkelplan.
        slots (pd.DataFrame): The DataFrame containing the information about the slots.
        rooms (pd.DataFrame): The DataFrame containing the information about the rooms.
        campdays (pd.DataFrame): The DataFrame containing the information about the campdays.
        beamer_available (int): The amount of available beamers.
        room_change_penalty_matrix (pd.DataFrame): The penalties for moving a beamer from one to another room.

    Returns:
        pd.DataFrame: The DataFrame containing the optimal usage of each beamer in each slot.
    """
    classes_with_beamer = (
        zirkelplan.query("Beamer == True")
        [["Slot", "Zirkel", "Zirkelleitende"]]
        .merge(rooms, left_on="Zirkel", right_on="Zirkel")
    )
    beamer_needed = classes_with_beamer["Slot"].value_counts().max()
    if beamer_needed > beamer_available:
        raise ValueError(f"Not enough beamer available: {beamer_needed} needed")
    beamer_list = pd.DataFrame(columns=["Slot"] + list(range(beamer_available)))
    for index_day in campdays.index:
        slots_for_beamer_plan_on_index_day = (
            slots.query("Camptag == @index_day")
            .query("Beamerplan")
            [["Slot"]]
            .reset_index(drop=True)
        )
        classes_with_beamer_on_index_day = (
            slots_for_beamer_plan_on_index_day.merge(classes_with_beamer, left_on="Slot", right_on="Slot", how="left")
            [["Slot", "Zirkel","Zirkelleitende","Raum"]]
            .reset_index(drop=True)
        )
        if classes_with_beamer_on_index_day.empty:
            continue
        G = make_beamer_allocation_graph(classes_with_beamer_on_index_day, room_change_penalty_matrix, beamer_available)
        beamer_allocation_on_day = get_beamer_allocation_by_graph(G, beamer_available)
        beamer_allocation_on_day = pd.concat([slots_for_beamer_plan_on_index_day, beamer_allocation_on_day], axis=1)
        beamer_list = pd.concat([beamer_list, beamer_allocation_on_day], axis=0)
    return beamer_list

def make_beamer_list_tex_cell(data: Dict) -> str:
    """
    The function creates TeX code to visualize a cell in the beamer list.

    Parameters:
        row (Dict): A dict containing the keys ["Zirkel", "Zirkelleitende", "Raum"]

    Returns:
        str: The TeX code representing a cell
    """
    cell_tex = r"\begin{tabular}{@{}ll@{}}" + \
rf"""{data["Zirkel"]}\\
{", ".join(data["Zirkelleitende"])}\\
{data["Raum"]}\\
\end{{tabular}}"""
    return cell_tex

def make_beamer_list_tex_row(row: pd.Series) -> str:
    """
    The function creates TeX code to visualize a row in the beamer list.

    Parameters:
        row (pd.Series): The Series containing the beamer usage of one slot.

    Returns:
        str: The TeX code representing a row
    """
    row_tex = \
rf"""
{row["Slot"]}
"""
    for i in range(len(row) - 1):
        row_tex += "&\n"
        if pd.notna(row[i]):
            row_tex += make_beamer_list_tex_cell(row[i])
            row_tex += "\n"
    row_tex += r"\\"
    return row_tex

def make_beamer_list_tex(beamer_list: pd.DataFrame) -> str:
    """
    The function creates TeX code to visualize the beamer list.

    Parameters:
        beamer_list (pd.DataFrame): The DataFrame containing the optimal usage of each beamer in each slot.

    Returns:
        str: The beamer list as a colored longtable in TeX
    """
    max_number_of_beamers = len(beamer_list.columns) - 1
    textbf = r"\textbf"
    output = \
rf"""\rowcolors{{1}}{{gray!20}}{{white}}
\begin{{longtable}}[l]{{|l|{"l|" * max_number_of_beamers}}}
\hline
\rowcolor{{gray!40}}
\textbf{{Slot}} & {" & ".join([f"{textbf}{{Beamer {i+1}}}" for i in range(max_number_of_beamers)])}\\
\hline
\endhead
"""
    for _, row in beamer_list.iterrows():
        output += make_beamer_list_tex_row(row)
        output += "\n"
    output += \
r"""\hline
\end{longtable}
"""
    return output
