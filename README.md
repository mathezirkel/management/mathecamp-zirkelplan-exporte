# Mathezirkel Organization Helper

**Deprecated: The functionality is not integrated in the [Mathezirkel App](https://gitlab.com/mathezirkel/mathezirkel-app/exports).**

## Beschreibung

Das Skript verarbeitet erstellt verschiedene Dateien für die Organisation des Mathezirkels

* Graphen für Zimmerwünsche und Zirkelwünsche
* Kontaktdateien
* Raumbelegung für die Zirkel
* Themen aus dem Zirkelplan extrahieren
* Chief-Aufgaben
* Stundenplan für die Betreuer:innen
* Optimierten Beamernutzungsplan

## Anleitung

### Konfigurationsdatei anpassen

Passe die Konfigurationsdatei entsprechend der gewünschten Outputs an.
Die Datei muss in `MC_CONFIG_DIR` liegen und den Namen `config.yaml` haben.

Ein Beispiel ist im Ordner [example](./example/) zu finden.

### Skript ausführen

```commandline
$ export MC_LIST_DIR=path/to/lists
$ export MC_DOCUMENT_DIR=path/to/documents
$ export MC_CONFIG_DIR=path/to/config/
$ export TAG=CHANGEME
$ docker run \
    -v $MC_LIST_DIR:/app/lists \
    -v $MC_DOCUMENT_DIR:/app/documents \
    -v $MC_CONFIG_DIR:/app/config/ \
    registry.gitlab.com/mathezirkel/management/organization-helper:$TAG
```

## Dokumentation

### Umgebungsvariablen

* MC_LIST_DIR: Pfad des Ordners, in dem sich die Exports der Mathezirkel-App befinden.
* MC_DOCUMENT_DIR: Pfad des Ordners, in die tex Dateien exportiert werden.
* MC_CONFIG_DIR: Pfad des Ordners, in dem sich die csv Dateien befinden bzw. exportiert werden.

Im Dockerfile werden die Umgebungsvariablen statisch gesetzt und als Volumes übergeben.

### Konfigurationsdatei

ACHTUNG: Dokumentation nicht aktuell!!!

* zirkelplan
  * sheet_id: ID des GoogleDocs Sheet mit dem Zirkelplan
  * sheet_name: Name des Tabellenblatts mit dem Zirkelplan
  * extra_columns: Liste an Spalten, die keinen Zirkel repräsentieren
  * infozirkel: Boolean, ob es einen Infozirkel gibt
  * skip_rows: Liste an Zeilen, die nicht eingelesen werden sollen. **Wichtig**: Es können nur Zeilen eingelesen werden, die entweder leer sind oder Zirkel enthalten.
  * choice_system: Teilnehmer:innen können spontan Zirkel wählen
    * key: Name des Zirkels in der Datenbank
    * value: Liste an Zirkeln die gewählt werden können (Spaltennamen im Spreadsheet)

* data: Die Dateipfade sind relativ zu $MC_LIST_DIR. Beispiele siehe [hier](./example/).

    | Variable | Beschreibung | Relevante Spalten | Notwendig für |
    |----------|--------------|-------------------|---------------|
    | teachers | Infos zu den Betreuenden | Kuerzel | alles |
    | slots | Liste an Slots | Slot, Camptag, Zeitraum, Urkunde, Beamer | alles |
    | participants | Infos zu den Teilnehmenden | Name, Vorname, Zirkel | Urkunden |
    | campdays | Liste an Camptagen | Index, Wochentag | Stundenplan, Beamer |
    | room_change_penalties | Gewichte zur Erstellung des Beamerplans. Je höher das Gewicht, desto schlechter ist ein Beamerwechsel zwischen den entsprechenden Räumen. | | Beamer |

* rooms:
  * output: Ordner für die Ausgabe der Raumbelegung

* certificates:
  * enabled: Boolean, um die Erstellung der Urkundendatei zu aktivieren
  * output: Ausgabe der CSV für die Urkundenerstellung relativ zu $MC_LIST_DIR

* chiefs:
  * enabled: Boolean, um die Erstellung der Chief-Listen zu aktivieren
  * html_file: Dateiname des Planungspads als HTML relativ zu $MC_LIST_DIR
  * heading_id:
    * chiefs: Überschrift im Pad, unter der die Chiefs aufgelistet sind.
    * fraktivitaeten_chiefs: Überschrift im Pad, unter der die Fraktivitäten-Chiefs aufgelistet sind.
  * output: Ausgabe der CSV Dateien der Chiefs relativ zu $MC_LIST_DIR

* timetable:
  * enabled: Boolean, um die Erstellung des Stundenplans zu aktivieren
  * output: Ausgabe der TeX-Datei relativ zu $MC_DOCUMENT_DIR

* beamer:
  * enabled: Boolean, um die Erstellung des Beamerplans zu aktivieren
  * available: Anzahl der verfügbaren Beamer
  * output: Ausgabe der TeX-Datei relativ zu $MC_DOCUMENT_DIR

## License
This project is licenced unter GPL-3.0, see [LICENSE](./LICENSE).
